#!/usr/bin/env python3

import rtlsdr
import numpy as np
import pandas as pd
import getopt
import sys


class AntennaData():
    def __init__(self):
        self.args = sys.argv[1:]

    def rtl(self, cfreq=1090e6, sample_rate=2.4e6, n=1):
        """
        the sample rate is set to maximum, as default, but is usually set in accordance
        to the intended frequency
        please also refer to the source code of the wrapper provided:
        https://github.com/roger-/pyrtlsdr/blob/master/rtlsdr/rtlsdr.py
        """
        sdr = rtlsdr.RtlSdr()
        """
        As described in the explanation in c't 8/2016 
        about the oscilloscope built with a raspberry pi, 
        the sample rate should be higher than the aim frequency.
        Due to the maximum sample rate of 2.4e6 for the deployed sdr
        devices, this sample rate is chosen for the software
        https://www.rtl-sdr.com/about-rtl-sdr/
        """
        sdr.sample_rate = sample_rate
        # sdr.sr is the same
        sdr.center_freq = cfreq 
        # sdr.fc is the same
        if sdr.freq_correction > 0:
            sdr.freq_correction = sdr.freq_correction * (-1)
        """
        defines the offset of the frequency measuring crystal 
        from the defined frequency
        """
        gains = sdr.get_gains
        """
        determined with rtl_test command for the R820T2
        
        this is a regulation for the signal to noise ration,
        where higher values mean higher selectivity of the 
        sdr device

        https://www.rtl-sdr.com/forum/viewtopic.php?t=2095
        """
        bs = int(np.around(np.log2(sample_rate), decimals=0))
        ra = np.power(2, bs)
        '''
        As the amount of samples recorded from the sdr device can only be 
        a power of 2 value, the logarithm of 2 and the corresponding power value
        have to be computed.
        If other amounts are deployed, the system terminates with an error.
        '''
        while n > 0:
            yield pd.Series(
                name='Measurement',
                data=sdr.read_samples(num_samples=ra)
            )
            n = n - 1
        '''
        https://www.reddit.com/r/RTLSDR/comments/skvdi/pyrtlsdr_another_python_wrapper_for_librtlsdr/
        '''

    def call_from_input(self, amount=5, sample_rate=2.4e6):
        """
        as the core function returns a generator, the execution of the 
        function takes place during the calls.
        Therefore, the runtime variable on the measurement rounds has to 
        be set in the controll structure
        """
        longargs = ["frequency=", "amount=", "help"]
        # these arguments have to be called with 
        # double dash and the equal sign
        try:
            optlist, arguments = getopt.getopt(
                args=self.args,
                shortopts='a:f:h:',
                # these are the abbreviated terms 
                # for the input parameters
                longopts=longargs
                # these are the long descriptions 
                # for the input parameters
                # please refer to:
                # https://docs.python.org/3/library/getopt.html
            )
            for v, o in optlist:
                if v in ('-f', '--frequency'):
                    # please refer the correct definition of call parameters to:
                    # https://pymotw.com/3/getopt/ and to
                    # https://docs.python.org/3/library/getopt.html
                    try:
                        freq = int(o)
                    except ValueError:
                        print('only integer values allowed for f')
                        return None
                if v in ('-a','--amount'):
                    try:
                        amount = int(o)
                    except ValueError:
                        print('only integer values allowed for a')
                        return None
                if v in ('-h', '--help'):
                    print("Allowed options are: -f and --frequency to set aim frequency and \n -a and --amount to set the amount of samplings")
                    return None
            try:
                path = "/home/pi/Agrartechnik/Results/" + np.datetime_as_string(np.datetime64('now')) + " Results.csv"
                res = pd.Series(name='Measurement')
                if (freq * 2) <= 2.4e6:
                    sample_rate = freq * 2
                    # sample rate is no longer a call parameter as this could lead to false results
                else:
                    sample_rate = 2.4e6
                    # this is the maximum sample rate, which can be processed by the sdr device
                # the locals() function contains all assigned variables of the current function
                if "freq" in locals() and "amount" in locals():
                    for x in self.rtl(cfreq=freq, sample_rate=sample_rate, n=amount):
                        res = pd.concat([res, x], axis=0)
                    res.to_csv(path, sep=",", na_rep=" ")
                    return res
                    # the calling function will request the return value
                    # as long as this value is 0, save function of the code is 
                    # assumed
                elif "amount" in locals():
                    for x in self.rtl(sample_rate=sample_rate, n=amount):
                        res = pd.concat([res, x], axis=0)
                    res.to_csv(path, sep=",", na_rep=" ")
                    return res
                elif "freq" in locals():
                    for x in self.rtl(cfreq=freq, sample_rate=sample_rate, n=amount):
                        res = pd.concat([res, x], axis=0)
                    res.to_csv(path, sep=",", na_rep=" ")
                    return res
                elif "freq" not in locals():
                    for x in self.rtl(sample_rate=sample_rate, n=amount):
                        # in this case, the default parameters are deployed
                        res = pd.concat([res, x], axis=0)
                    res.to_csv(path, sep=",", na_rep=" ")
                    return res

            except OSError:
                return 1
        except getopt.GetoptError:
            print('Argument error')
            return None

if __name__ == '__main__':
    AntennaData().call_from_input()
    '''
    this allows for the file to be called in the following syntax:
    python3 -m chosen_function input_values
    '''
