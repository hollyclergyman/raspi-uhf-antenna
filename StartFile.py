#!/usr/bin/python3

from subprocess import run
import sys
import getopt
import os
import getpass


class Caller():
    def __init__(self):
        self.args = sys.argv[1:]

    def check_for_folder(self, designation_folder):
        user = getpass.getuser()
        try:
            fpath = "/home/" + user + designation_folder
            os.path.isdir(fpath)
            return fpath
        except FileNotFoundError:
            return None

    def arguments(self):
        longargs = ["frequency=", "amount=", "help"]
        optlist, arguments = getopt.getopt(
                args=self.args,
                shortopts='a:f:h',
                # these are the abbreviated terms 
                # for the input parameters
                longopts=longargs
                # these are the long descriptions 
                # for the input parameters
                # please refer to:
                # https://docs.python.org/3/library/getopt.html
            )
        r = {}
        for v, o in optlist:
            if v in ("-a", "--frequency"):
                try:
                    r["a"] = int(o)
                except ValueError:
                    return None
            if v in ("-f", "--frequency"):
                try:
                    r["f"] = int(o)
                except ValueError:
                    return None
            if v in ("-h", "--help"):
                print(
                    "Allowed options are: -f and --frequency to set aim frequency and \n -a and --amount to set the amount of samplings"
                    )
                return None

    def execution(self, designation_folder="/Agrartechnik"):
        fpath = self.check_for_folder(designation_folder)
        if fpath is None:
            return None
        args = self.arguments()
        if args is None:
            args = {"a": int(1), "f": int(1090000000)}
        os.chdir(fpath)
        r = run(
            args=[
                "pipenv", 
                "run", 
                "python3", 
                "Code/RequestAntenna.py", 
                "-a",
                str(args["a"]),
                "-f",
                str(args["f"])
                ]
            )
        # run is the more recent environment to execute external code in python
        # https://docs.python.org/3/library/subprocess.html#subprocess.run
        sys.exit(0)
        # https://stackoverflow.com/questions/18231415/best-way-to-return-a-value-from-a-python-script
    
if __name__ == "__main__":
    Caller().execution()
