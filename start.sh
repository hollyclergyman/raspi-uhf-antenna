#/bin/bash

while getopts ":a:f:" opt; do
    case $opt in
    a)
        a=$OPTARG
        # no idea, why the variable initiation is without $ character...
    ;;
    f)
        f=$OPTARG
    ;;
    esac
# this script is the script to be called for the python3 script to be executed
# please refer the code to:
# http://wiki.bash-hackers.org/howto/getopts_tutorial
done
echo $a, $f
cd Agrartechnik/
pipenv run python3 Code/RequestAntenna.py -a $a -f $f
exit
